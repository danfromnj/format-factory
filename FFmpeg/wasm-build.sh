emcc -Llibavcodec -Llibavdevice -Llibavfilter -Llibavformat -Llibavresample -Llibavutil -Llibpostproc -Llibswscale -Llibswresample \
	-Qunused-arguments -Oz \
	-o ../wasm/ffmpeg-base.js fftools/ffmpeg_opt.o fftools/ffmpeg_filter.o fftools/ffmpeg_hw.o fftools/cmdutils.o fftools/ffmpeg.o fftools/file_tools.o \
	-lavdevice -lavfilter -lavformat -lavcodec -lswresample -lswscale -lavutil -lm \
	-s MODULARIZE=1 \
	-s EXPORTED_FUNCTIONS='["_wasmmain","_testmain","_getFileList","_setIterations"]' \
	-s EXTRA_EXPORTED_RUNTIME_METHODS='["cwrap", "FS", "getValue", "setValue"]' \
	-s TOTAL_MEMORY=512MB \
	-s ALLOW_MEMORY_GROWTH=1
