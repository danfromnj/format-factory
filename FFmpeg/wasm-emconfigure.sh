emconfigure ./configure --cc="emcc" --ar="emar" --ranlib=emranlib --disable-stripping \
	--prefix=$(pwd)/../dist --enable-cross-compile --target-os=none --arch=x86_64 --cpu=generic --disable-ffplay --disable-ffprobe \
	--disable-asm --disable-doc --disable-devices --disable-pthreads --disable-w32threads --disable-network --disable-hwaccels \
	--disable-parsers --disable-bsfs --disable-debug --disable-protocols --disable-indevs --disable-outdevs --enable-protocol=file
