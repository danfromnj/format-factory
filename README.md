## Build FFmpeg to Web assembly

cd FFmpeg

emconfigure ./configure --cc="emcc" --ar="emar" --ranlib=emranlib --disable-stripping \
	--prefix=$(pwd)/../dist --enable-cross-compile --target-os=none --arch=x86_64 --cpu=generic --disable-ffplay --disable-ffprobe \
	--disable-asm --disable-doc --disable-devices --disable-pthreads --disable-w32threads --disable-network --disable-hwaccels \
	--disable-parsers --disable-bsfs --disable-debug --disable-protocols --disable-indevs --disable-outdevs --enable-protocol=file

emmake make

emcc -Llibavcodec -Llibavdevice -Llibavfilter -Llibavformat -Llibavresample -Llibavutil -Llibpostproc -Llibswscale -Llibswresample \
	-Qunused-arguments -Oz \
	-o ../wasm/ffmpeg-base.js fftools/ffmpeg_opt.o fftools/ffmpeg_filter.o fftools/ffmpeg_hw.o fftools/cmdutils.o fftools/ffmpeg.o fftools/file_tools.o \
	-lavdevice -lavfilter -lavformat -lavcodec -lswresample -lswscale -lavutil -lm \
	-s MODULARIZE=1 \
	-s EXPORTED_FUNCTIONS='["_wasmmain","_testmain","_getFileList"]' \
	-s EXTRA_EXPORTED_RUNTIME_METHODS='["cwrap", "FS", "getValue", "setValue"]' \
	-s TOTAL_MEMORY=512MB \
	-s ALLOW_MEMORY_GROWTH=1



## Run WebAssembly in nodejs

cd node

node mp4-to-avi.js ../videos/accident.mp4 ./output.avi

mkdir frames
node frame-capture.js ../videos/accident.mp4 ./frames



## Run WebAssembly Chrome Extension

Enable chrome developer mode
copy ffmpeg-base.js and ffmpeg-base.wasm to chrome-extension/main
load unpack extension in chrome-extension folder
use extension



