function b64toBlob(b64Data, contentType='video/mp4', sliceSize=512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
  
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
  
    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
}
  
function arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return btoa(binary);
    // return window.btoa(binary);
}

// ugly... show find a unified way as it's in nodejs env
function str2ptr(Module,s){
    const ptr = Module._malloc((s.length+1)*Uint8Array.BYTES_PER_ELEMENT);
    for (let i = 0; i < s.length; i++) {
            Module.setValue(ptr+i, s.charCodeAt(i), 'i8');
          }
    Module.setValue(ptr+s.length, 0, 'i8');
    return ptr;
}

// ugly... show find a unified way as it's in nodejs env
function strList2ptr(Module, strList){
    const listPtr = Module._malloc(strList.length * Uint32Array.BYTES_PER_ELEMENT);

    strList.forEach((s, idx) => {
            const strPtr = str2ptr(Module, s);
            Module.setValue(listPtr + (4 * idx), strPtr, 'i32');
          });

    return listPtr;
};
