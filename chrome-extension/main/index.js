const $uploadVid = document.querySelector('#upload-vid');

const worker = new Worker('./worker.js');

var curIndex = 0;
var base64_img_array = null;
var timer = null;

worker.onmessage = function(e) {
	console.log("Received execution result from worker for command " + e.data.cmd);
	if(e.data.cmd === "testwasm"){
		document.getElementById("workerRet").value = e.data.result;
	}
	else if(e.data.cmd === "mp42avi"){
		var blob = b64toBlob(e.data.result);
		saveAs(blob, "converted.avi");
	}
	else if(e.data.cmd === "capture"){
		const msg = e.data;

		base64_img_array = e.data.frames;

		if(e.data.recurring){
			if(timer != null){
				clearInterval(timer);
				timer = null;
			}
			curIndex = 0;
			timer = setInterval(()=>{
				if(base64_img_array != null){
					if(curIndex >= base64_img_array.length){
						curIndex = 0;
					}
					base64_img = base64_img_array[curIndex++];
	
					addImage("frame_img",base64_img);
					//drawImageCanvas(base64_img);
				}
			}, 500);
		}
		else{
			for(let i = 0; i < base64_img_array.length; ++i){
				addImage("frame_img_" + i, base64_img_array[i]);
			}
		}
	}
	else if(e.data.cmd === "benchmark"){

	}
}

document.getElementById("btn_mp42avi").addEventListener("click", function() {
	worker.postMessage({cmd:"mp42avi", base64_data:localStorage.getItem("data_mp4")});
	document.getElementById("btn_mp42avi").disabled = true;
	},
false);

document.getElementById("btn_capture").addEventListener("click", function() {
	const _start = document.getElementById('start_capture').value;
	const _end = document.getElementById('end_capture').value;
	const _fps = document.getElementById('fps').value;
	const _recurring = document.getElementById('recurring').checked;

	worker.postMessage({cmd:"capture", start:_start, end:_end, fps:_fps, recurring: _recurring, base64_data:localStorage.getItem("data_mp4")});
	document.getElementById("btn_capture").disabled = true;
	},
false);

document.getElementById("btn_benchmark").addEventListener("click", function() {
	console.log("benchmark");
	const _iterations = document.getElementById('iterations').value;
	worker.postMessage({cmd:"benchmark", iterations:_iterations, base64_data:localStorage.getItem("data_mp4")});
	document.getElementById("btn_benchmark").disabled = true;
	},
false);


document.getElementById("btn_testWebWorker").addEventListener("click", function() {
	worker.postMessage({cmd:"testwasm"});
	},
false);

$uploadVid.onchange = (e) => {
  const f = e.target.files[0];
  var fileReader = new FileReader();
  fileReader.onload = function (e) {
  	  var base64_encoded = arrayBufferToBase64(fileReader.result);
		localStorage.setItem("data_mp4", base64_encoded);
		document.getElementById("btn_mp42avi").disabled = false;
		document.getElementById("btn_capture").disabled = false;
		document.getElementById("btn_benchmark").disabled = false;
   }
   fileReader.readAsArrayBuffer(f);

}

function addImage(id,base64Data){

	var node = document.getElementById(id);
	if(node == null){
		var div_node = document.createElement("div");
		var p_node = document.createElement("p");
		div_node.appendChild(p_node);
		var img_node = document.createElement("img");
		img_node.setAttribute("id", id);
		img_node.setAttribute("src","data:image/jpg;base64, " + base64Data);
		img_node.setAttribute("alt", "nothing");
		div_node.appendChild(img_node);
		document.body.appendChild(div_node);
	}
	else{
		node.setAttribute("src","data:image/jpg;base64, " + base64Data);
	}
}

function drawImageCanvas(base64Data){

	var c = document.getElementById("playCanvas");
	var ctx = c.getContext("2d");
	var img = new Image();
	img.src = "data:image/jpg;base64, " + base64Data;
	ctx.drawImage(img, 0, 0);
}