self.importScripts("./ffmpeg-base.js")
self.importScripts("./encoder.js")

self.Module().then(M=>{
        self.Module = M;
        self.testwasm = self.Module.cwrap('testmain', 'number', ['number', 'number']);
        self.ffmpeg = self.Module.cwrap('wasmmain', 'number', ['number', 'number']);
        self.setIterations = self.Module.cwrap('setIterations', 'number', ['number']);
        console.log("web assembly loaded.");
});

self.onmessage = function(e) {
    if(typeof self.ffmpeg === "undefined") {
       postMessage({ERROR:"Web Assembly is not ready!"});
       return;
    }

    console.log("Received request from main script command = " + e.data.cmd);

    if(e.data.cmd === "mp42avi"){
        const base64_data = e.data.base64_data;
        const data = b64toBlob(base64_data);

        data.arrayBuffer().then(buffer => {
            const raw_data = new Uint8Array(buffer);
            const temp_memfs_file = 'in.mp4';
            const temp_memfs_out_file = 'out.avi';
    
            self.Module.FS.writeFile(temp_memfs_file, raw_data);
            const args = ['./ffmpeg', '-i', temp_memfs_file, temp_memfs_out_file];
            self.ffmpeg(args.length, strList2ptr(self.Module, args));

            const out_data = Module.FS.readFile(temp_memfs_out_file);

            const out_data_b64 = arrayBufferToBase64(out_data);
            this.postMessage({cmd:"mp42avi", result:out_data_b64});
        });
    }
    else if(e.data.cmd === "capture"){
        const base64_data = e.data.base64_data;
        const data = b64toBlob(base64_data);
        const start = e.data.start;
        const end = e.data.end;
        const fps = e.data.fps;
        const recurring = e.data.recurring;

        data.arrayBuffer().then(buffer => {
            const raw_data = new Uint8Array(buffer);
            const temp_memfs_file = 'in.mp4';
            const temp_memfs_folder = "/datas";

            self.Module.FS.mkdir(temp_memfs_folder);
            self.Module.FS.writeFile(temp_memfs_file, raw_data);
            const args = ['./ffmpeg', '-ss', start, '-to', end, '-i', temp_memfs_file, '-s', '640x360', '-vf', 'fps=' + fps, "-benchmark", temp_memfs_folder + '/out%05d.jpg'];
            self.ffmpeg(args.length, strList2ptr(Module, args));
            const file_list = getFileListWasm(self.Module, temp_memfs_folder, ".jpg");
            console.log("Total frames captured : " + file_list.length);

            msg = {cmd:"capture", frames:[]};
            msg["recurring"] = recurring;
            file_list.forEach(s=>{
                const img_file_path = temp_memfs_folder + "/" + s;
                const out_data_b64  = arrayBufferToBase64(Module.FS.readFile(img_file_path));
                msg.frames.push(out_data_b64);
                Module.FS.unlink(img_file_path);
            });

            this.postMessage(msg);
        });
    }
    else if(e.data.cmd === "testwasm"){
        this.postMessage({cmd: e.data.cmd, result: self.testwasm()});
    }
    else if(e.data.cmd === "benchmark"){
        const iteration = e.data.iteration;
        const data = b64toBlob(e.data.base64_data);

        data.arrayBuffer().then(buffer=>{
            const raw_data = new Uint8Array(buffer);
            const temp_memfs_file = 'in.mp4';
            self.Module.FS.writeFile(temp_memfs_file, raw_data);
            const args = ['./ffmpeg', '-ss', '00:00:00', '-to', '00:00:05', '-i', temp_memfs_file, '-s', '1920x1080', '-vf', 'fps=32', "-benchmark","-f", "null", './out%05d.jpg'];
            console.log(self.setIterations(e.data.iterations));

            self.ffmpeg(args.length, strList2ptr(Module,args));
            this.postMessage({cmd: e.data.cmd});
        });
    }
}


// ugly... show find a unified way as it's in nodejs env
function getFileListWasm(Module, dir, ext)  {
	const MAX_FILE_NUM = 100;
	const MAX_PATH_LEN = 256;
	var buf_arr = Module._malloc(MAX_FILE_NUM * Uint32Array.BYTES_PER_ELEMENT);
	for(i = 0; i < 100; ++i) {
		var buf = Module._malloc(MAX_PATH_LEN * Uint8Array.BYTES_PER_ELEMENT);
		Module.setValue(buf_arr + (4 * i), buf, 'i32');
	};
	
	const getFileList = Module.cwrap('getFileList','number', ['number', 'number','number', 'number']);
	let files = [];
	const file_num = getFileList(buf_arr, MAX_FILE_NUM, str2ptr(Module,dir), str2ptr(Module, ext));
	for(i = 0; i < file_num; ++i) {
		const arr = Module.getValue(buf_arr + i * 4, 'i32'); 
		var s = ""; 
		for(j = 0; j < MAX_PATH_LEN; j++)
		{
			const v = Module.getValue(arr + j,'i8');
			if(v == 0) break; 
			s += String.fromCharCode(v);
		};
		files.push(s);
    }
	
	for(i = 0;i < MAX_FILE_NUM; ++i){
		var ptr = Module.getValue(buf_arr + i * 4, 'i32');
		Module._free(ptr);
	}
	Module._free(buf_arr);
	return files;
}