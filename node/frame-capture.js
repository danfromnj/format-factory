/*
var cmd_args = process.argv.slice(2);
const local_mp4 = cmd_args[0];
const local_output = cmd_args[1];

const local_mp4 = "../videos/accident.mp4";
const local_output = "./frames";
const Module =  require("../wasm/ffmpeg-base.js")();
const stringListToPointerArray = require('./stringListToPointerArray');
const ffmpeg = Module.cwrap('wasmmain', 'number', ['number', 'number']);
let fs = require("fs");
const data = new Uint8Array(fs.readFileSync(local_mp4));
const temp_memfs_file = 'in.mp4';
const temp_memfs_folder = "/datas";
Module.FS.writeFile(temp_memfs_file, data);
Module.FS.mkdir(temp_memfs_folder);
const args = ['./ffmpeg', '-ss', '00:00:00', '-to', '00:00:05', '-i', temp_memfs_file, '-vf', 'fps=8', temp_memfs_folder + '/out%05d.jpg'];
ffmpeg(args.length, stringListToPointerArray(Module,args));
const getFileListWasm = require("./getFileListWasm");
const file_list = getFileListWasm(Module, temp_memfs_folder, ".jpg");
file_list.forEach(s=>{const data  = Buffer.from(Module.FS.readFile(temp_memfs_folder + "/" + s)); fs.writeFileSync(local_output + "/" + s,data);});
*/

// mkdir frames
// node frame-capture.js ../videos/accident.mp4 ./frames


var cmd_args = process.argv.slice(2);
const local_mp4 = cmd_args[0];
const local_output = cmd_args[1];

require('../wasm/ffmpeg-base.js')().then(Module => {
	let fs = require("fs")
	const data = new Uint8Array(fs.readFileSync(local_mp4));
	const temp_memfs_file = 'in.mp4';
	const temp_memfs_folder = "/datas";
	const stringListToPointerArray = require('./stringListToPointerArray');
	Module.FS.writeFile(temp_memfs_file, data);
	Module.FS.mkdir(temp_memfs_folder);
	const args = ['./ffmpeg', '-ss', '00:00:00', '-to', '00:00:05', '-i', temp_memfs_file, '-vf', 'fps=8', temp_memfs_folder + '/out%05d.jpg'];
	const ffmpeg = Module.cwrap('wasmmain', 'number', ['number', 'number']);
	ffmpeg(args.length, stringListToPointerArray(Module,args));
	const getFileListWasm = require("./getFileListWasm");
	const file_list = getFileListWasm(Module, temp_memfs_folder, ".jpg");
	file_list.forEach(s=>{const data  = Buffer.from(Module.FS.readFile(temp_memfs_folder + "/" + s)); fs.writeFileSync(local_output + "/" + s,data);});
	});