/* usage:
 const M = require("./file_tools.js")()
 const d = new Uint8Array(fs.readFileSync('./hello.c'));
 M.FS.mkdir("/datas")
 M.FS.writeFile("/datas/d1.c", d);
 M.FS.writeFile("/datas/d2.c", d);
 M.FS.writeFile("/datas/d3.c", d);
 const getFileListWasm = require("./getFileListWasm")
 const arr = getFileListWasm(M,"/datas",".c");
 arr.forEach(s=>{console.log(s);})
*/

module.exports = (Module, dir, ext) => {
	const MAX_FILE_NUM = 100;
	const MAX_PATH_LEN = 256;
	var buf_arr = Module._malloc(MAX_FILE_NUM * Uint32Array.BYTES_PER_ELEMENT);
	for(i = 0; i < 100; ++i) {
		var buf = Module._malloc(MAX_PATH_LEN * Uint8Array.BYTES_PER_ELEMENT);
		Module.setValue(buf_arr + (4 * i), buf, 'i32');
	};
	
	const getFileList = Module.cwrap('getFileList','number', ['number', 'number','number', 'number']);
	files = [];
	const stringToPointer = require("./stringToPointer");
	const file_num = getFileList(buf_arr, MAX_FILE_NUM, stringToPointer(Module,dir), stringToPointer(Module, ext));
	for(i = 0; i < file_num; ++i) {
		const arr = Module.getValue(buf_arr + i * 4, 'i32'); 
		var s = ""; 
		for(j = 0; j < MAX_PATH_LEN; j++)
		{
			const v = Module.getValue(arr + j,'i8');
			if(v == 0) break; 
			s += String.fromCharCode(v);
		};
		files.push(s);
	}
	
	for(i = 0;i < MAX_FILE_NUM; ++i){
		var ptr = Module.getValue(buf_arr + i * 4, 'i32');
		Module._free(ptr);
	}
	Module._free(buf_arr);
	return files;
};
