// const local_mp4 = '../videos/accident.mp4';
// const local_output_avi = './output.avi';
// const temp_memfs_file = 'in.mp4';
// const temp_memfs_out_file = 'out.avi';
// const data = new Uint8Array(fs.readFileSync(local_mp4));
// const Module =  require("../wasm/ffmpeg-base.js")();
// Module.FS.writeFile('in.mp4', data);
// const args = ['./ffmpeg', '-i', temp_memfs_file, temp_memfs_out_file];
// const ffmpeg = Module.cwrap('wasmmain', 'number', ['number', 'number']);
// const stringListToPointerArray = require('./stringListToPointerArray');
// ffmpeg(args.length, stringListToPointerArray(Module,args));

// const out_data = Buffer.from(Module.FS.readFile('out.avi'))
// fs.writeFileSync(local_output_avi, out_data)


// usage:  node mp4-to-avi.js ../videos/accident.mp4 ./output.avi
var cmd_args = process.argv.slice(2);
const local_mp4 = cmd_args[0];
const local_output_avi = cmd_args[1];

require('../wasm/ffmpeg-base.js')().then(Module => {
	let fs = require("fs")
	const data = new Uint8Array(fs.readFileSync(local_mp4));
	const temp_memfs_file = 'in.mp4';
	const temp_memfs_out_file = 'out.avi';
	const stringListToPointerArray = require('./stringListToPointerArray');
	Module.FS.writeFile(temp_memfs_file, data);
	const args = ['./ffmpeg', '-i', temp_memfs_file, temp_memfs_out_file];
	const ffmpeg = Module.cwrap('wasmmain', 'number', ['number', 'number']);
	ffmpeg(args.length, stringListToPointerArray(Module,args));
	const out_data = Buffer.from(Module.FS.readFile(temp_memfs_out_file));
	fs.writeFileSync(local_output_avi, out_data);
});