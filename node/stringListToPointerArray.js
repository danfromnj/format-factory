const stringToPointer = require('./stringToPointer');

module.exports = (Module, strList) => {
	const listPtr = Module._malloc(strList.length * Uint32Array.BYTES_PER_ELEMENT);
	strList.forEach((s, idx) => {
		      const p = stringToPointer(Module, s);
		      Module.setValue(listPtr + (4 * idx), p, 'i32');
	});
	return listPtr;
};