// comiple with gcc:
// g++ fibonacci.cpp -o fibonacci
// compile for direct use:
// emcc fibonacci.cpp -s WASM=1 -o fibonacci.js 
// compile for modularize:
// emcc fibonacci.cpp -s WASM=1 -s MODULARIZE=1  -s EXPORTED_FUNCTIONS='["_fibonacci"]' -s EXTRA_EXPORTED_RUNTIME_METHODS='["cwrap"]' -o fibonacci.js



#include <iostream>
#include <string>

extern "C" int fibonacci(int n)
{
	if(n < 1) return 0;
	if(n < 3) return 1;

	int n1 = 1;
	int n2 = 1;
	while(n >= 3)
	{
		int ret = n1 + n2;
		if(--n < 3) return ret;
		n1 = n2;
		n2 = ret;
	}
}

int main(int argc, char** argv) 
{
	if(argc <= 1){
		std::cout << "argument missing!" << std::endl;
		return -1;
	}

	try{
		const int index = std::stoi(argv[1]);
		std::cout << "fibonacci(" << index << ") = " 
		<< fibonacci(index) << std::endl;
	}
	catch (const std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return -2;
    }

	return 0;
}
