// emcc file_tools.c -s WASM=1 -s MODULARIZE=1 -s EXPORTED_FUNCTIONS='["_getFileList"]' -s EXTRA_EXPORTED_RUNTIME_METHODS='["cwrap", "FS", "getValue", "setValue"]' -o file_tools.js
// gcc file_tools.c -o file_tools

#include <dirent.h> 
#include <stdio.h> 
#include <string.h>
#include <stdlib.h>
#include "file_tools.h"

const static int MAX_PATH = 256;
const static int MAX_FILE = 100;

int getFileList(char** path_array, int max_file, const char* cszDir, const char* cszExt) 
{
  struct dirent *dir = NULL;
  int file_num = 0;
  DIR* d = opendir(cszDir);
  if (d) 
  {
    while (file_num < max_file && (dir = readdir(d)) != NULL) 
    {
    	if(dir->d_name[0] == '.') continue;
    	if(!strstr(dir->d_name, cszExt)) continue;
      	strncpy(path_array[file_num], dir->d_name, MAX_PATH - 1);
      	file_num++;
    }
    closedir(d);
  }
  return file_num;
}

// NOTICE: main may be eliminated as dead code if "_main" is not in EXPORTED_FUNCTIONS in emcc compile options.
/*
int main(int argc, char** argv)
{
	// allocate memories
	char** path_array = malloc(MAX_FILE * sizeof(char*));
	for(int i = 0;i < MAX_FILE; ++i)
	{
		path_array[i] = malloc(MAX_PATH);
		memset(path_array[i], 0 , MAX_PATH);
	}
	
	int file_num = getFileList(path_array, MAX_FILE, argv[1], argv[2]);
	for(int i = 0; i < file_num; ++i)
	{
		printf("%s\n", path_array[i]);
	}
	
	// free all memories
	for(int i = 0; i < MAX_FILE; ++i)
	{
		free(path_array[i]);
		path_array[i] = NULL;
	}
	free(path_array);
	path_array = NULL;
	return 0;
}
*/